import PyPDF2
import docx

pdfFileObj = open('F:\OnlineMaterials\meetingminutes.pdf', 'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
print(pdfReader.isEncrypted)
print(pdfReader.numPages)
pageObj = pdfReader.getPage(0)
print(pageObj.extractText())

document = docx.Document('F:\OnlineMaterials\demo.docx')
print(len(document.paragraphs))

def getText(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)

print(getText('F:\OnlineMaterials\demo.docx'))